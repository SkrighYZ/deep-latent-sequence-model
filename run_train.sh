#!/bin/bash
#SBATCH -J dlsm_train
#SBATCH --partition=gpu 
#SBATCH --output=../slurm_output/dlsm/output_train.txt
#SBATCH --error=../slurm_output/dlsm/error_train.txt
#SBATCH -t 120:00:00
#SBATCH -c 8 
#SBATCH -p gpu  --gres=gpu:1 -C K80
#SBATCH --begin now
#SBATCH --mail-type all
​
module load anaconda3
export CUDA_VISIBLE_DEVICES=0
source activate dlsm2
​

python src/main.py \
  --dataset hate \
  --clean_mem_every 2 \
  --reset_output_dir \
  --classifier_dir="pretrained_classifer/hate" \
  --train_src_file data/hate/train.txt \
  --train_trg_file data/hate/train.attr \
  --dev_src_file data/hate/dev.txt \
  --dev_trg_file data/hate/dev.attr \
  --dev_trg_ref data/hate/dev.txt \
  --src_vocab  data/hate/text.vocab \
  --trg_vocab  data/hate/attr.vocab \
  --d_word_vec=128 \
  --d_model=512 \
  --log_every=100 \
  --eval_every=1500 \
  --ppl_thresh=10000 \
  --eval_bleu \
  --batch_size 8 \
  --valid_batch_size 8 \
  --patience 5 \
  --lr_dec 0.5 \
  --lr 0.001 \
  --dropout 0.3 \
  --max_len 10000 \
  --seed 0 \
  --beam_size 1 \
  --word_blank 0. \
  --word_dropout 0. \
  --word_shuffle 0. \
  --cuda \
  --anneal_epoch 3 \
  --temperature 0.01 \
  --max_pool_k_size 5 \
  --bt \
  --bt_stop_grad \
  --klw 0.1 \
  --lm \
  --avg_len \


​
echo "run_train.sh complete."