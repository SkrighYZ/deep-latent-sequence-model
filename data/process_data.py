outdir = 'hate'
indir = 'raw'

'''
filelist = ['sentiment.dev.0', 'sentiment.dev.1', 'sentiment.test.0', 'sentiment.test.1', 'sentiment.train.0', 'sentiment.train.1']
vocab = set()

for file in filelist:
	with open(indir + '/' + file, 'r') as f:
		for line in f:
			line = line.lower().encode('ascii', 'ignore').decode()
			line = line.replace('"', '')
			line = line.replace('.', '')
			line = line.replace(',', '')
			line = line.replace('?', '')
			line = line.replace('!', '')
			line = line.replace(';', '')
			line = line.replace(':', '')
			line = line.replace('\'', '')
			line = line.replace('\t', '')
			line = line[:-1]
			words = line.split(' ')
			for word in words:
				vocab.add(word)

with open(outdir + '/text.vocab', 'w') as f:
	for word in list(vocab):
		f.write(word + '\n')


'''


dataset = 'test'
file0 = indir + '/' + 'sentiment.' + dataset + '.0'
file1 = indir + '/' + 'sentiment.' + dataset + '.1'
attr0 = outdir + '/' + dataset + '_0.attr'
attr1 = outdir + '/' + dataset + '_1.attr'
text0 = outdir + '/' + dataset + '_0.txt'
text1 = outdir + '/' + dataset + '_1.txt'
attr = outdir + '/' + dataset + '.attr'
text = outdir + '/' + dataset + '.txt'

fin0 = open(file0, 'r')
fout_attr0 = open(attr0, 'w')
fout_text0 = open(text0, 'w')
fout_attr = open(attr, 'w')
fout_text = open(text, 'w')
for line in fin0:
	line = line.lower().encode('ascii', 'ignore').decode()
	line = line.replace('"', '')
	line = line.replace('.', ' .')
	line = line.replace(',', ' ,')
	line = line.replace('?', ' ?')
	line = line.replace('!', ' !')
	line = line.replace(';', ' ;')
	line = line.replace(':', ' :')
	line = line.replace('\'', '')
	line = line.replace('\t', '')
	fout_attr0.write('negative\n')
	fout_attr.write('negative\n')
	fout_text0.write(line)
	fout_text.write(line)
fin0.close()
fout_attr0.close()
fout_text0.close()

fin1 = open(file1, 'r')
fout_attr1 = open(attr1, 'w')
fout_text1 = open(text1, 'w')
for line in fin1:
	line = line.lower().encode('ascii', 'ignore').decode()
	line = line.replace('"', '')
	line = line.replace('.', ' .')
	line = line.replace(',', ' ,')
	line = line.replace('?', ' ?')
	line = line.replace('!', ' !')
	line = line.replace(';', ' ;')
	line = line.replace(':', ' :')
	line = line.replace('\'', '')
	line = line.replace('\t', '')
	fout_attr1.write('positive\n')
	fout_attr.write('positive\n')
	fout_text1.write(line)
	fout_text.write(line)
fin1.close()
fout_attr1.close()
fout_text1.close()

fout_attr.close()
fout_text.close()

