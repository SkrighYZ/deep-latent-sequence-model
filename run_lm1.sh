#!/bin/bash
#SBATCH -J dlsm_lm1
#SBATCH --partition=gpu 
#SBATCH --output=../slurm_output/dlsm/output.txt%j
#SBATCH --error=../slurm_output/dlsm/error.txt%j
#SBATCH -t 120:00:00
#SBATCH -c 8 
#SBATCH -p gpu  --gres=gpu:1 -C K80
#SBATCH --begin now
#SBATCH --mail-type all
​
module load anaconda3
export CUDA_VISIBLE_DEVICES=0
source activate dlsm
​
python src/lm_lstm.py \
    --dataset hate \
    --style 1

​
echo "run_lm1.sh complete."